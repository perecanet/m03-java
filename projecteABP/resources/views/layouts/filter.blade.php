<div class="col-lg-3 search-product">
    <br /><br /><br />
    <h1 class="my-4">Shop Name</h1>
    <div class="list-group">
        <a href="{{ action([App\Http\Controllers\ProductController::class, 'category'], ['category'.'='.'1']) }}" class="list-group-item">Category 1</a>
        <a href="{{ action([App\Http\Controllers\ProductController::class, 'category'], ['category'.'='.'2']) }}" class="list-group-item">Category 2</a>
        <a href="{{ action([App\Http\Controllers\ProductController::class, 'category'], ['category'.'='.'3']) }}" class="list-group-item">Category 3</a>
    </div>


    <form role="form" id="filterSearch" method="post">
        @csrf
        <div class="form-group">
        <div class="form-check">
        <input class="form-check-input" type="checkbox" value="1" name="stars" id="stars">
        <label class="form-check-label" for="stars">1 estrella</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="2" name="stars" id="stars">
            <label class="form-check-label" for="stars">2 estrellas</label>
        </div>
        <!-- Poner 5 checkbox de 1 a 5 estrellas-->
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<script>
    $('#filterSearch').submit(function(e) {
        e.preventDefault()
        var data = $('#filterSearch').serialize()
        axios.post('products',data)
            .then(response => {
                console.log(response)
                $('#content').replaceWith(response.data)
            })
    })
</script>

<!--<form id="filterFormRestaurant" role="form" method="post">
                            
    <button data-filter="pizzes">Pizzes</button>
    <input type="checkbox" class="form-check-input" value="pizza" name="category" {{ (old('pizza'))?'checked':'' }}>
-->
