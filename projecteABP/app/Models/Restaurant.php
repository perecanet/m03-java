<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    public function scopeDescomptes() {
        return $this->where('discount','=','1')->get();
    }
    public function scopeMenus() {
        return $this->where('menu','=','1')->get();
    }
    public function scopeLotes() {
        return $this->where('lots','=','1')->get();
    }
    public function scopeTipus($query,$tipus) {
        return $query->where('type','=',$tipus)->get();
    }
    public function scopeSearch($query,$search){

        $query1 = $query->where('name','LIKE','%'.$search.'%')->orWhere('type','LIKE','%'.$search.'%')->get();

        return $query1;
    }
    public function scopeAll($query){
        $query1 = $query->where('name','LIKE','%')->get();
        return $query1;
    }
}
