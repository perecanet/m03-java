<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeAll($query) {
        $querry1 = $query->where('name','LIKE','%');
        return $querry1;
    }
    
    public function scopeCategory($query,$cat) {
        return $query->where('category','=', $cat);
    }

    public function scopeStars($query,$star) {
        return $query->where('stars','=',$star);
    }

}
