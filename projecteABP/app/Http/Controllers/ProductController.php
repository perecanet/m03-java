<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{    
   
    private $products;
    private $_filters;

    public function __construct()
    {
        /**
         * Filters (name=>value) format to show in the view
         * Write the content of the stars
         */
        $this->product = new Product();
        //$this->_filters=(object)array(
          //  'category'=>array('Categori1'=>'cat1','Categori2'=>'cat3','Categori3'=>'cat3'),
            //'stars'=>array()
        //);
    }
    /**
     * Method to list all the products
     */
    public function all()
    {   
        $products = new Product();
        $products->all();
        return view('examenViews.products')->with('products', $products->get());

    }

    /**
     * Method to list the products filtered by category
     */
    public function category(Request $request)
    {   
        $products = $this->product->query(); 
        
        $products->category($request->input('category'));
        
        return view('examenViews.products')->with('products', $products->get());
    }

    /**
     * Method to list the products filtered by stars
     */
    public function stars(Request $request)
    {
        $products = $this->product->query(); 
        
        $products->stars($request->input('stars'));
        
        return view('examenViews.products')->with('products', $products->get());

    }
}