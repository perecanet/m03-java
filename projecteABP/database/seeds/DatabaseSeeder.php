<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductSeeder::class);
    }
}

class ProductSeeder extends Seeder
{
    public function run(){
        DB::table('products')->insert([
            'name'=> 'Product 1',
            'price'=> 20,
            'description'=> 'prod 1',
            'category'=> 1,
            'stars'=> 1,
        ]);

        DB::table('products')->insert([
            'name'=> 'Product 2',
            'price'=> 25,
            'description'=> 'prod 2',
            'category'=> 2,
            'stars'=> 2,
        ]);

        DB::table('products')->insert([
            'name'=> 'Product 3',
            'price'=> 30,
            'description'=> 'prod 3',
            'category'=> 1,
            'stars'=> 2,
        ]);

        DB::table('products')->insert([
            'name'=> 'Product 4',
            'price'=> 35,
            'description'=> 'prod 4',
            'category'=> 3,
            'stars'=> 1,
        ]);

        DB::table('products')->insert([
            'name'=> 'Product 5',
            'price'=> 15,
            'description'=> 'prod 5',
            'category'=> 3,
            'stars'=> 2,
        ]);
    }
}