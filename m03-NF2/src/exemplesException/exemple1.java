package exemplesException;

public class exemple1 {

	public static void main(String[] args) {
		int opcio;
		try {
			opcio = Integer.parseInt(args[0]);
			
			switch(opcio) {
				case 0: System.out.println("Has introduït 0");
					break;
				case 1: System.out.println("Has introduït 1");
					break;
				default: System.out.println("????");
			}
		} catch (Exception e) {
			System.out.println("Error en els paràmetres --> " + args[0]);
			System.out.println("Estem dins el bloc catch que ha capturat l’excepció .");
			System.out.println("Informació que dona el mètode getMessage():");
			System.out.println(e.getMessage());
			System.out.println("Informació que dona el mètode printtStackTrace():");
			e.printStackTrace();
			System.out.print("Informació donada pel mètode toString():");
			System.out.println(e.toString());
			System.exit(0);
		} finally {
			System.out.println( "Final: sempre s'executa, hagi o no un error" );
		}
	}

}
