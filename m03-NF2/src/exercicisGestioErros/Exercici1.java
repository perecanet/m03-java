package exercicisGestioErros;

public class Exercici1 {
	static void Ex1() {
		String t[]={"Hola","Adéu","Fins demà"};
		try {
			System.out.println("Abans d’executar el for");
			for (int i=0; i<=t.length; i++)
				System.out.println("Posició " + i + " : " + t[i]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Error");
		} finally {
			System.out.println("Final");
		}
	}
}
