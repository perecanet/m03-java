package exercicisGestioErros;

import java.util.Scanner;

public class Exercici0 {
	static void Ex0() {
		System.out.println("----Entra un id----");
		Scanner sc = new Scanner(System.in);
		int user_id=sc.nextInt();
		sc.close();
		try{
			if (user_id!=1234){
				throw new InvalidUserIdException();
			}
		} catch (Exception e){
			System.out.println(e);
		}
	}
	/*
  	int current_balance = 1000;
  	System.out.println("----Entra l’ ingrès a dipositar ----");
  	Scanner cs = new Scanner(System.in);
  	int deposit_amount=cs.nextInt();
  	cs.close();
  	try{
  	   if(deposit_amount<0){
  	       throw new NegativeNotAllowedException();
  	   }else{
  	       current_balance+=deposit_amount;
  	       System.out.println("Updated balance is: "+current_balance);

  	   }
  	}catch (Exception e){
  	   System.out.println(e);
  	}
  	*/
}
