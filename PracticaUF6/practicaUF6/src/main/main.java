package main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import entities.Client;
import entities.Comanda;
import entities.Producte;
import dao.ClientJDBCDAO;
import dao.ComandaJDBCDAO;
import dao.ProducteJDBCDAO;
import dao.DAOException;

public class main {
	public static void main(String[] args) throws DAOException, SQLException, IOException {

		// 1. JDBC
		//Clientes
		System.out.println("get all clients");
		ClientJDBCDAO test = new ClientJDBCDAO();
		boolean conection = test.connect();
		System.out.println(conection ? "Connection stablished" : "Unable to connect");
		if (test.isConnected()) {
			// .0 get all clients
			System.out.println("Print clientes table");
			test.printTablaClientes();

			System.out.println("1.0 -- get client by id (9)");
			// .1 get client by id
			System.out.println(test.getClientById(8));
			
			System.out.println("1.0 -- update Client with id 5");
			System.out.println(test.updateCliente(5, "Joan", "Anglada") ?"Client updtaed successfully": "No columns affected");
			System.out.println("Verifying update.");
			System.out.println(test.getClientById(5));
			
			
			System.out.println("Inserting client"); test.insertCliente("Joan","Pons");
			System.out.println("Verifying insert");
			test.printTablaClientes();
			
			System.out.println("Deleting client"); 
			test.deleteCliente(2);
			System.out.println(test.getClientById(2) == null? "Client deleted sucessfully" : "Client not deleted");			
			
			test.close();
			
			System.out.println(test.isConnected()? "Problem closing connection" : "Connection closed sucessfully");
			
			// COMANDES AMB JOIN

			System.out.println("JOIN");
			
			ComandaJDBCDAO com = new ComandaJDBCDAO();
			boolean conection2 = com.connect();
			System.out.println(conection2 ? "Connection stablished" : "Unable to connect");
			if (com.isConnected()) {
			
				System.out.println("PRINTING ORDERS");
				
				com.prtingComandes(com.getComandes());
				com.close();
				System.out.println(com.isConnected()? "Problem closing connection" : "Connection closed sucessfully");
			}
		}
		//Productes
				System.out.println("get all Productes");
				ProducteJDBCDAO test3 = new ProducteJDBCDAO();
				boolean conection3 = test3.connect();
				System.out.println(conection ? "Connection stablished" : "Unable to connect");
				if (test.isConnected()) {
					// .0 get all clients
					System.out.println("Print Productes table");
					test3.printTablaProductes();

					System.out.println("get Producte by id (4)");
					// .1 get Productes by id
					System.out.println(test3.getProducteById(4));
					
					System.out.println("1.0 -- update Productes with id 6");
					System.out.println(test3.updateProducte(6, "Altaveu", "4") ?"Producte updtaed successfully": "No columns affected");
					System.out.println("Verifying update.");
					System.out.println(test3.getProducteById(6));
					
					
					System.out.println("Inserting Producte"); test3.insertProducte("Auriculars","7");
					System.out.println("Verifying insert");
					test3.printTablaProductes();
					
					System.out.println("Deleting Producte"); 
					test3.deleteProducte(2);
					System.out.println(test3.getProducteById(2) == null? "Producte deleted sucessfully" : "Producte not deleted");			
					
					test3.close();
					
					System.out.println(test3.isConnected()? "Problem closing connection" : "Connection closed sucessfully");
				}
	}
}
