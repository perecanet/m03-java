package dao;

import java.sql.DriverManager;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import entities.Client;
import dao.ClientDAO;
import dao.DAOException;
import utils.JDBCUtils;

import java.sql.ResultSet;

/**
 *
 * @author eva
 * TODO
 * Cal definir els contractes de la interfície ClientDAO i demés entitats, i implementar-les 
 * Cal crear de la mateixa manera, ProducteJDBCDAO que implementa ProducteDAO, i idem per a ComandaJDBCDAO
 */
public class ClientJDBCDAO implements ClientDAO {

    // Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3306";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "admin";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    // Configuración de la tabla Clientes
    private static final String DB_CLI = "clientes";
    private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
    private static final String DB_CLI_ID = "id";
    private static final String DB_CLI_NOM = "nombre";
    private static final String DB_CLI_DIR = "direccion";

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     * @throws DAOException 
     * @throws SQLException 
     * @throws IOException 
     */
    public static boolean connect() throws DAOException, SQLException, IOException {
    	
    	
        try {
        	
        	//conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/tienda?serverTimezone=Europe/Paris", "root", "admin");
            //sentSQL = connection.prepareStatement("SELECT id, name, red, green, blue FROM colors WHERE id= ?");
        	conn = JDBCUtils.openConnection();
            
        }
        catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
        
        return conn.isValid(10);
    	
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     * @throws SQLException 
     */
    public static boolean isConnected() throws SQLException {
        return !conn.isClosed();
    }

    /**
     * Cierra la conexión con la base de datos
     * @throws SQLException 
     */
    public static void close() throws SQLException {
        conn.close();
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE TABLA CLIENTES
    //////////////////////////////////////////////////
    ;
    
    // Devuelve 
    // Los argumentos indican el tipo de ResultSet deseado
    /**
     * Obtiene toda la tabla clientes de la base de datos
     * @param resultSetType Tipo de ResultSet
     * @param resultSetConcurrency Concurrencia del ResultSet
     * @return ResultSet (del tipo indicado) con la tabla, null en caso de error
     */
    public static ResultSet getTablaClientes(int resultSetType, int resultSetConcurrency) {
        return null;

    }

    /**
     * Obtiene toda la tabla clientes de la base de datos
     *
     * @return ResultSet (por defecto) con la tabla, null en caso de error
     * @throws DAOException 
     */
    public static ResultSet getTablaClientes() throws DAOException {        
        try (Connection connection = JDBCUtils.openConnection();
             CallableStatement sentSQL = connection.prepareCall("CALL getClients()")) {
           
            //sentSQL.setLong(1, id);
            try (ResultSet reader = sentSQL.executeQuery()) {
            	return reader;
            }
        }
        catch (SQLException | IOException ex) {
            //Logger
            throw new DAOException(ex);
        }
    }

    /**
     * Imprime por pantalla el contenido de la tabla clientes
     * @throws DAOException 
     */
    public static void printTablaClientes() throws DAOException {
    	
    	Client client = null;
		ArrayList<Client> clCol = new ArrayList<Client>();
        
        try (CallableStatement sentSQL = conn.prepareCall("CALL getClients()")) {
           
            //sentSQL.setLong(1, id);
            try (ResultSet reader = sentSQL.executeQuery()) {
                while (reader.next()) {
                    // ORM: [--,--,--,--,--,--] -----> []Color
                	clCol.add(JDBCUtils.getClient(reader));
                }            
            }
        }
        catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
        clCol.forEach(el -> System.out.println(el));
        
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE UN SOLO CLIENTE
    //////////////////////////////////////////////////
    ;
    
    /**
     * Solicita a la BD el cliente con id indicado
     * @param id id del cliente
     * @return ResultSet con el resultado de la consulta, null en caso de error
     */
    public static ResultSet getCliente(int id) {
        return null;
    }

    /**
     * Comprueba si en la BD existe el cliente con id indicado
     *
     * @param id id del cliente
     * @return verdadero si existe, false en caso contrario
     * @throws DAOException 
     */
    public static boolean existsCliente(int id) throws DAOException {
    	//Color color = null;
		Client client = null;        
        try (CallableStatement sentSQL = conn.prepareCall("CALL getClientById(?)")) {
        	// bind param
            sentSQL.setInt(1, id);
            try (ResultSet reader = sentSQL.executeQuery()) {
                while (reader.next()) {
                    // ORM: [--,--,--,--,--,--] -----> []Color
                	client = JDBCUtils.getClient(reader);
                }            
            }
        }
        catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
        return client != null;
    }

    /**
     * Imprime los datos del cliente con id indicado  --> Carga objecto Client
     *
     * @param id id del cliente
     * @throws DAOException 
     */
    public static void printCliente(int id) throws DAOException {
    	//Color color = null;
    			Client client = null;        
    	        try (CallableStatement sentSQL = conn.prepareCall("CALL getClientById(?)")) {
    	        	// bind param
    	            sentSQL.setInt(1, id);
    	            try (ResultSet reader = sentSQL.executeQuery()) {
    	                while (reader.next()) {
    	                    // ORM: [--,--,--,--,--,--] -----> []Color
    	                	client = JDBCUtils.getClient(reader);
    	                }            
    	            }
    	        }
    	        catch (SQLException ex) {
    	            //Logger
    	            throw new DAOException(ex);
    	        }
    	         System.out.println(client.toString());
    }

    /**
     * Solicita a la BD insertar un nuevo registro cliente
     *
     * @param nombre nombre del cliente
     * @param direccion dirección del cliente
     * @return verdadero si pudo insertarlo, false en caso contrario
     * @throws DAOException 
     */
    public static boolean insertCliente(String nombre, String direccion) throws DAOException {
    	int result = 0;
    	//Color color = null;
    			Client client = null;        
    	        try (CallableStatement sentSQL = conn.prepareCall("CALL insertClient(?,?)")) {
    	        	// bind param
    	            sentSQL.setString(1, nombre);
    	            sentSQL.setString(2, direccion);
    	            
    	            result  = sentSQL.executeUpdate();
    	        }
    	        catch (SQLException ex) {
    	            //Logger
    	            throw new DAOException(ex);
    	        }
    	
    	
        return result != 0;
    }

    /**
     * Solicita a la BD modificar los datos de un cliente
     *
     * @param id id del cliente a modificar
     * @param nombre nuevo nombre del cliente
     * @param direccion nueva dirección del cliente
     * @return verdadero si pudo modificarlo, false en caso contrario
     * @throws DAOException 
     */
    public boolean updateCliente(int id, String nuevoNombre, String nuevaDireccion) throws DAOException {
    	int result = 0;
    	try (CallableStatement sentSQL = conn.prepareCall("CALL updateClient(?,?,?)")) {
           	// bind param
               sentSQL.setInt(1, id);
               sentSQL.setString(2, nuevoNombre);
               sentSQL.setString(3, nuevaDireccion);
               result  = sentSQL.executeUpdate();
        } catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
    	
    	return result != 0;
    }

    /**
     * Solicita a la BD eliminar un cliente
     *
     * @param id id del cliente a eliminar
     * @return verdadero si pudo eliminarlo, false en caso contrario
     * @throws DAOException 
     */
    public static boolean deleteCliente(int id) throws DAOException {
    	
    	int result = 0;
    	//Color color = null;
    			Client client = null;        
    	        try (CallableStatement sentSQL = conn.prepareCall("CALL deleteClient(?)")) {
    	        	// bind param
    	            sentSQL.setInt(1, id);
    	            
    	            result  = sentSQL.executeUpdate();
    	        }
    	        catch (SQLException ex) {
    	            //Logger
    	            throw new DAOException(ex);
    	        }
    	
    	
        return result != 0;
    }
    
	@Override
	public Client getClientById(int id) throws DAOException {
		Client client = null;        
        try (CallableStatement sentSQL = conn.prepareCall("CALL getClientById(?)")) {
        	// bind param
            sentSQL.setInt(1, id);
            try (ResultSet reader = sentSQL.executeQuery()) {
                while (reader.next()) {
                    // ORM: [--,--,--,--,--,--] -----> []Color
                	client = JDBCUtils.getClient(reader);
                }            
            }
        }
        catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
        return client;
	}

	@Override
	public ArrayList<Client> getClients() throws DAOException {
		Client client = null;
		ArrayList<Client> clCol = new ArrayList<Client>();
        
        try (CallableStatement sentSQL = conn.prepareCall("CALL getClients()")) {
           
            //sentSQL.setLong(1, id);
            try (ResultSet reader = sentSQL.executeQuery()) {
                while (reader.next()) {
                    // ORM: [--,--,--,--,--,--] -----> []Color
                	clCol.add(JDBCUtils.getClient(reader));
                }            
            }
        }
        catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
        return clCol;
	}
	
}