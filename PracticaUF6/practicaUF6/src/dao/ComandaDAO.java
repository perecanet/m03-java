package dao;

import java.util.ArrayList;
import entities.Comanda;

public interface ComandaDAO {

	ArrayList<Comanda> getComandes() throws DAOException;
}

