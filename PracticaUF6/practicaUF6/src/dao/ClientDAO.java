package dao;

import java.util.ArrayList;
import java.util.List;
import dao.DAOException;
import entities.Client;

public interface ClientDAO {

	//TO-DO crea el contracte o llista de mètodes a oferir
	
	Client getClientById(int id) throws DAOException;
	ArrayList<Client> getClients() throws DAOException;
	boolean updateCliente(int id, String nombre, String direccion) throws DAOException;
	    
}