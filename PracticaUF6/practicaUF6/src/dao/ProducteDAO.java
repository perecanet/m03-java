package dao;

import java.util.ArrayList;
import java.util.List;
import dao.DAOException;
import entities.Producte;

public interface ProducteDAO {

	//TO-DO crea el contracte o llista de mètodes a oferir
	
	Producte getProducteById(int id) throws DAOException;
	ArrayList<Producte> getProductes() throws DAOException;
	boolean updateProducte(int id, String nombre, String stock) throws DAOException;
	    
}