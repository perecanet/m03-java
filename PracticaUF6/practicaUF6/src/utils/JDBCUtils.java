package utils;

import entities.Client;
import entities.Comanda;
import entities.Producte;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public final class JDBCUtils {

    
    private JDBCUtils() {
    }

    public static Connection openConnection() throws SQLException, IOException {
        Properties props = new Properties();
        props.load(new FileReader("config/jdbc.properties"));
        return DriverManager.getConnection(props.getProperty("mysql.url"),
                                           props.getProperty("mysql.username"),
                                           props.getProperty("mysql.password"));
    }
    
    public static Client getClient(ResultSet reader) throws SQLException {
        Client c =  new Client(reader.getInt("id"), reader.getString("nombre"), reader.getString("direccion"));
        //cliente = new Client(reader.getInt("id"), reader.getString("nombre"), reader.getString("direccion"));
        //c.setId(reader.getLong("id"));
        return c;
     }
    
    
    public static Comanda getComandes(ResultSet reader) throws SQLException {
    	Comanda c =  new Comanda(reader.getString("client"), reader.getString("producte"),
    			reader.getInt("quantitat"), reader.getDouble("preu_unitari"), reader.getDouble("total"));
        //cliente = new Client(reader.getInt("id"), reader.getString("nombre"), reader.getString("direccion"));
        //c.setId(reader.getLong("id"));
        return c;
     }

	public static Producte getProducte(ResultSet reader) throws SQLException {
		Producte c =  new Producte(reader.getInt("id"), reader.getString("nombre"), reader.getString("stock"));
        
        return c;
     }
}
