package entities;

public class Producte {

	private long id;
	private String nombre;
	private String stock;
	
	
	public Producte(long id, String nombre, String stock) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.stock = stock;
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	
	@Override
	public String toString() {
		return "Producte [id=" + id + ", nombre=" + nombre + ", stock=" + stock + "]";
	}
		
}

